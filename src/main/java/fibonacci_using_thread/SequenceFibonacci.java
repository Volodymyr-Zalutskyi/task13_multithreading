package fibonacci_using_thread;

import java.util.Map;
import java.util.concurrent.*;

public class SequenceFibonacci {
    static ExecutorService executor = Executors.newFixedThreadPool(2);
    // Memoized map.
    private static Map<Integer, Long> mem = new ConcurrentHashMap<>();

    public static void main(String[] args) throws ExecutionException, InterruptedException{
        final long then = System.nanoTime();


        System.out.println(fib_wrapper(10));

        final long millis = TimeUnit.NANOSECONDS.toMillis(System.nanoTime() - then);
        System.out.println("Time(ms): " + millis);
        executor.shutdown();
    }

    public static int fib_wrapper(int n) throws ExecutionException, InterruptedException {
        Future<Integer> future = executor.submit(() -> fib(n-1));
        Future<Integer> future2 = executor.submit(() -> fib(n-2));

        return future2.get() + future.get();
    }

    public static int fib(int n) throws ExecutionException, InterruptedException {
        if (n == 1)
            return 1;
        if (n == 0)
            return 0;

        if (mem.containsKey(n)) {
            return Math.toIntExact(mem.get(n));
        }

        long x = fib(n-1) + fib(n-2);
        mem.put(n, x);
        return (int) x;
    }
}
