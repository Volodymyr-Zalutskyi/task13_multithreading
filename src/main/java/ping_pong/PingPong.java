package ping_pong;

import ping_pong.Constants;

import java.time.LocalDateTime;

import static java.lang.System.out;

public class PingPong {
    public static void show() throws InterruptedException {
        Thread pongThread = new Thread(new PongRun());
        Thread pingThread = new Thread(new PingRun());
        pongThread.setName("pong-thread");
        pongThread.setName("ping-thread");
        out.println(LocalDateTime.now());;
        pongThread.start();
        pingThread.start();

        final long start = System.nanoTime();

        pingThread.join();
        pongThread.join();


        final long duration = System.nanoTime() - start;

        out.printf("duration %,d (ns)%n", duration);
        out.printf("%,d ns/op%n", duration / (Constants.MAX * 2));
        out.printf("%,d ops/s%n", (Constants.MAX * 2 * 10) / duration);
        out.println("pingValue = " + Constants.PING + ", pongValue = " + Constants.PONG);
        out.println(LocalDateTime.now());;
    }
}
